# -*- coding: utf-8 -*-
"""
Created on Thu May 14 15:00:18 2020

@author: ASUS
"""

import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder ,StandardScaler
dataset =pd.read_excel('C:/Users/ASUS/PIBI/RLogistique.xlsx')

le =LabelEncoder()
y=dataset['isActif']
features=['mouvement_libelle','montant','duree']
x=dataset[features]
s  = StandardScaler()

x=s.fit_transform(x)

#split and train the datset
x_train,x_test,y_train,y_test = train_test_split(x,y)
log=LogisticRegression()

log.fit(x_train,y_train)
y_pred =log.predict(x)
y_prob= log.predict_proba(x)[:,1]
dataset['predictions']=y_pred
dataset['probabilities']=y_prob
