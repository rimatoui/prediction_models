
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 15:00:18 2020

@author: ASUS
"""

import pandas as pd
import numpy as np

dataset =pd.read_excel('C:/Users/ASUS/Desktop/data2.xlsx')


from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder ,StandardScaler
le =LabelEncoder()


y=dataset['Class']
features=['Gender', 'Nationality', 'Major', 'Level', 'IE1', 'SMSK3', 'RAS1',
       'RAS2', 'SMSK1', 'SMSK4', 'IE2', 'TL1', 'RAS3', 'IE3', 'RAS4', 'RAS5',
       'IE4', 'SMSK2', 'TL2', 'TL3', 'PSD1', 'PSD2', 'PSD3', 'IE5', 'PSD4',
       'PSD5', 'IM1', 'IM2', 'IM3', 'IM4', 'IM5', 'IM6', 'W1', 'W2', 'W3',
       'Employed', 'Score']

x=dataset[features]
s = StandardScaler()
x =s.fit_transform(x)

#split and train the datset
x_train,x_test,y_train,y_test = train_test_split(x,y)
log=LogisticRegression()

log.fit(x_train,y_train)
y_pred =log.predict(x)
y_prob= log.predict_proba(x)[:,1]
dataset['predictions']=y_pred
dataset['probabilities']=y_prob
y_pred

from sklearn.metrics import f1_score
f1_score(y, y_pred, average='macro')

from sklearn.metrics import confusion_matrix
confusion_matrix = confusion_matrix(y, y_pred)
print(confusion_matrix)

from sklearn.metrics import classification_report
print(classification_report(y, y_pred))

from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from matplotlib import pyplot as plt
logit_roc_auc = roc_auc_score(y_test, log.predict(x_test))
fpr, tpr, thresholds = roc_curve(y_test, log.predict_proba(x_test)[:,1])
plt.figure()
plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.savefig('Log_ROC')
plt.show()
